# lab09

## How to run
1. Run the script below in the following folders: `lab09A-loadbalancer`, `lab09B-webserver`, `lab09C-webserver`
```bash
$ ./build-tag.sh
```
2. Go back to the folder `lab09` (the folder with the `docker-compose.yml` file) and run
```bash
$ docker-compose up
```
3. Test the loadbalancer
```bash
$ curl --insecure https://$(./get-loadbalancer-ip.sh | tr -d '"' )
or
$ curl --insecure -L http://$(./get-loadbalancer-ip.sh | tr -d '"' )
```
4. Tear everything back down with
```bash
$ docker-compose down
```

## docker-compose.yml
- Used to bring all container up together and link them.

### lab09{b,c}-webserver
- The two webservers which serve an `index.html` page with their hostnames.

#### index.html
- Webpage containing hardcoded hostname

#### Dockerfile
- Base image: **nginx:latest**
- Copies `index.html` to `/usr/share/nginx/html/index.html`
- Exposes port `80`

#### build-tag.sh
- Builds image
- Tags image

### lab09a-loadbalancer
- The loadbalancer that divides traffic between `lab09b-webserver` and `lab09c-webserver`
  - It uses `links` to make other containers available by the hostnames `lab09{b,c}`.
- You can get its IP addess by running the provided script
```bash
$ ./get-loadbalancer-ip.sh
```

#### nginx.conf
- Contains configuration for a loadbalancer for `lab09b` and `lab09c`.

#### Dockerfile
- Base image: **nginx:latest**
- Copies `nginx.conf` to `/etc/nginx/nginx.conf`
- Exposes port `80`

#### build-tag.sh
- Builds image
- Tags image
