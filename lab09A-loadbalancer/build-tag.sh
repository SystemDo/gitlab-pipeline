#!/bin/sh
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout localhost.key -out localhost.crt -subj "/C=BE/ST=Antwerp/L=Antwerp/O=Inuits/OU=Inuits/CN=lab02a"
ls -al
docker build -t registry.gitlab.com/systemdo/gitlab-pipeline/lab09a-loadbalancer .
